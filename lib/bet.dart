import 'dart:convert';

import 'package:mcbook/math.dart';

class Bet {
  String homeTeam = '';
  String awayTeam = '';

  int homeMLOdds = -110;
  int awayMLOdds = -110;

  double homeSpread = 0;
  int spreadOdds = -110;

  double overUnder = 0;
  int overUnderOdds = -110;

  double wager = 0;

  Bet({
    this.homeTeam,
    this.awayTeam,
    this.homeMLOdds,
    this.awayMLOdds,
    this.homeSpread,
    this.spreadOdds,
    this.overUnder,
    this.overUnderOdds,
    this.wager,
  });

  /// https://www.sportsbettingdime.com/guides/betting-101/how-to-read-sports-odds/
  ///
  double get overUnderPayout => getPayout(overUnderOdds, wager);
  double get spreadPayout => getPayout(spreadOdds, wager);
  
  double get awayMLPayout => getPayout(awayMLOdds, wager);
  double get homeMLPayout => getPayout(homeMLOdds, wager);

  double get overUnderImpliedProbility => getImpliedProbility(overUnderOdds);
  double get spreadImpliedProbility => getImpliedProbility(spreadOdds);

  double get awayMLImpliedProbility => getImpliedProbility(awayMLOdds);
  double get homeImpliedProbility => getImpliedProbility(homeMLOdds);

  Bet copyWith({
    String homeTeam,
    String awayTeam,
    int homeMLOdds,
    int awayMLOdds,
    double homeSpread,
    int spreadOdds,
    double overUnder,
    int overUnderOdds,
    double wager,
  }) {
    return Bet(
      homeTeam: homeTeam ?? this.homeTeam,
      awayTeam: awayTeam ?? this.awayTeam,
      homeMLOdds: homeMLOdds ?? this.homeMLOdds,
      awayMLOdds: awayMLOdds ?? this.awayMLOdds,
      homeSpread: homeSpread ?? this.homeSpread,
      spreadOdds: spreadOdds ?? this.spreadOdds,
      overUnder: overUnder ?? this.overUnder,
      overUnderOdds: overUnderOdds ?? this.overUnderOdds,
      wager: wager ?? this.wager,
    );
  }

  Map<String, dynamic> toMap() {
    return {
      'homeTeam': homeTeam,
      'awayTeam': awayTeam,
      'homeMLOdds': homeMLOdds,
      'awayMLOdds': awayMLOdds,
      'homeSpread': homeSpread,
      'spreadOdds': spreadOdds,
      'overUnder': overUnder,
      'overUnderOdds': overUnderOdds,
      'wager': wager,
    };
  }

  static Bet fromMap(Map<String, dynamic> map) {
    if (map == null) return null;

    return Bet(
      homeTeam: map['homeTeam'],
      awayTeam: map['awayTeam'],
      homeMLOdds: map['homeMLOdds'],
      awayMLOdds: map['awayMLOdds'],
      homeSpread: map['homeSpread'],
      spreadOdds: map['spreadOdds'],
      overUnder: map['overUnder'],
      overUnderOdds: map['overUnderOdds'],
      wager: map['wager'],
    );
  }

  String toJson() => json.encode(toMap());

  static Bet fromJson(String source) => fromMap(json.decode(source));

  @override
  String toString() {
    return 'Bet homeTeam: $homeTeam, awayTeam: $awayTeam, homeMLOdds: $homeMLOdds, awayMLOdds: $awayMLOdds, homeSpread: $homeSpread, spreadOdds: $spreadOdds, overUnder: $overUnder, overUnderOdds: $overUnderOdds, wager: $wager';
  }

  @override
  bool operator ==(Object o) {
    if (identical(this, o)) return true;

    return o is Bet &&
        o.homeTeam == homeTeam &&
        o.awayTeam == awayTeam &&
        o.homeMLOdds == homeMLOdds &&
        o.awayMLOdds == awayMLOdds &&
        o.homeSpread == homeSpread &&
        o.spreadOdds == spreadOdds &&
        o.overUnder == overUnder &&
        o.overUnderOdds == overUnderOdds &&
        o.wager == wager;
  }

  @override
  int get hashCode {
    return homeTeam.hashCode ^
        awayTeam.hashCode ^
        homeMLOdds.hashCode ^
        awayMLOdds.hashCode ^
        homeSpread.hashCode ^
        spreadOdds.hashCode ^
        overUnder.hashCode ^
        overUnderOdds.hashCode ^
        wager.hashCode;
  }
}
