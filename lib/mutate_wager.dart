import 'package:flutter/material.dart';
import 'package:mcbook/bet.dart';

class MutateWagerPage extends StatefulWidget {
  @override
  _MutateWagerPageState createState() => _MutateWagerPageState();
}

class _MutateWagerPageState extends State<MutateWagerPage> {
  final _formKey = GlobalKey<FormState>();
  Bet _bet;

  @override
  void initState() {
    super.initState();
    _bet = Bet();
  }

  @override
  Widget build(BuildContext context) {
    final headline = Theme.of(context).textTheme.display3;
    return Scaffold(
      appBar: AppBar(
        title: Text('Create a Slip'),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 75,
                      child: Text('   '),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.awayTeam,
                        onSaved: (value) {
                          _bet.awayTeam = value;
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(hintText: 'Away Team'),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Away Team';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('@'),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.homeTeam,
                        onSaved: (value) {
                          _bet.homeTeam = value;
                        },
                        keyboardType: TextInputType.text,
                        decoration: InputDecoration(hintText: 'Home Team'),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Home Team';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 75,
                      child: Text('ML'),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.awayMLOdds?.toString() ?? '',
                        onSaved: (value) {
                          _bet.awayMLOdds = int.parse(value);
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(hintText: '+/- ML'),
                        validator: (value) {
                          if (int.tryParse(value) == null) {
                            return 'Enter Away ML';
                          }
                          return null;
                        },
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(' '),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.homeMLOdds?.toString() ?? '',
                        onSaved: (value) {
                          _bet.homeMLOdds = int.parse(value);
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(hintText: '+/- ML'),
                        validator: (value) {
                          if (int.tryParse(value) == null) {
                            return 'Enter Home ML';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 75,
                      child: Text('PTS (-110)'),
                    ),
                    Flexible(
                      child: TextFormField(
                        decoration:
                            InputDecoration(hintText: '+/- Home Team PTS'),
                        onSaved: (value) {
                          _bet.homeSpread = double.parse(value);
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter Home Team Spread';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 75,
                      child: Text('TOT (-110)'),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.overUnder?.toString() ?? '',
                        onSaved: (value) {
                          _bet.overUnder = double.parse(value);
                        },
                        keyboardType: TextInputType.number,
                        decoration: InputDecoration(hintText: 'Over/Under'),
                        validator: (value) {
                          if (double.tryParse(value) == null) {
                            return 'Enter Over/Under';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Container(
                      width: 75,
                      child: Text('Wager'),
                    ),
                    Flexible(
                      child: TextFormField(
                        initialValue: _bet.overUnder?.toString() ?? '',
                        onSaved: (value) {
                          _bet.wager = double.parse(value);
                        },
                        keyboardType: TextInputType.number,
                        decoration:
                            InputDecoration(hintText: 'Wager. ie. 5.00'),
                        validator: (value) {
                          if (double.tryParse(value) == null) {
                            return 'Enter wager, no \$';
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
              ),
              RaisedButton(
                onPressed: () {
                  if (_formKey.currentState.validate()) {
                    _formKey.currentState.save();
                    _bet.spreadOdds = -110;
                    _bet.overUnderOdds = -110;

                    setState(() {});
                  }
                },
                child: Text('Save'),
              ),
              Padding(
                padding: const EdgeInsets.all(81.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          _bet.awayTeam ?? 'Away',
                          style: headline,
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            '@',
                            style: headline,
                          ),
                        ),
                        Text(
                          _bet.homeTeam ?? 'Home',
                          style: headline,
                        ),
                      ],
                    ),
                    Text('Wager: ${_bet.wager ?? '0'}'),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('Away ML: ${_bet.awayMLOdds ?? '0'}'),
                          Text('Payout: \$${_bet.awayMLPayout ?? '0'}'),
                          Text(
                              'Probability: ${_bet.awayMLImpliedProbility ?? '0'}'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('Home ML: ${_bet.homeMLOdds ?? '0'}'),
                          Text('Payout: \$${_bet.homeMLPayout ?? '0'}'),
                          Text(
                              'Probability: ${_bet.homeImpliedProbility ?? '0'}'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('Spread: ${_bet.homeSpread ?? '0'}'),
                          Text('Payout: \$${_bet.spreadPayout ?? '0'}'),
                          Text(
                              'Probability: ${_bet.spreadImpliedProbility ?? '0'}'),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Text('O/U: ${_bet.overUnder ?? '0'}'),
                          Text('Payout: \$${_bet.overUnderPayout ?? '0'}'),
                          Text(
                              'Probability: ${_bet.overUnderImpliedProbility ?? '0'}'),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
