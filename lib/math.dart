double getImpliedProbility(int odds) {
    if (odds == null || odds == 0) {
    return 0;
  }
  if (odds < 0) {
    return odds.abs() / (odds.abs() + 100) * 100;
  }
  return (100 / (odds.abs() + 100)) * 100;
}

double getPayout(int odds, double wager) {
  if (odds == null || odds == 0 || wager == null || wager == 0) {
    return 0;
  }
  if (odds < 0) {
    return ((wager * 100) / odds.abs()).roundToDouble();
  }
  return ((wager * odds.abs()) / 100).roundToDouble();
}
